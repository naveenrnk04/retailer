import React, { Component } from 'react';
import { Input, Select, Button } from 'antd';

//styles + config
import styles from './BeverageOrderForm.module.css';
import beverageMenuItemList from '../config/beverageMenu';

class BeverageOrderForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            beverageId: '',
            beverageName: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChangeName = event => {
        const { value } = event.target;
        this.setState({
            name: value
        })
    }

    handleChangeBeverage = event => {
        this.setState({
            beverageId: event.key,
            beverageName: event.label
        })
    }

    handleSubmit() {
        this.props.history.push({
            pathname: `/`,
            query: this.state
        })
    }

    render() {
        const { Option } = Select;
        return (
            <div className={styles.base}>
                <div className={styles.alignmentContent}>
                    <div className={styles.header}> Order Your Beverage</div>
                    <div className={styles.form}>
                        <div className={styles.flexRow}>
                            <div className={styles.name}>Name :</div>
                            <div className={styles.input}><Input placeholder="Name" value={this.state.name} onChange={this.handleChangeName} /></div>
                        </div>
                        <div className={styles.flexRow}>
                            <div className={styles.name}>Beverage :</div>
                            <div className={styles.select}>
                                <Select
                                    labelInValue
                                    defaultValue={{ key: 'Select' }}
                                    style={{ width: '100%' }}
                                    onChange={this.handleChangeBeverage}
                                >
                                    {beverageMenuItemList.map((data, i) => (
                                        <Option value={data.BeverageId} key={i}>{data.Name}</Option>
                                    ))}
                                </Select>
                            </div>
                        </div>
                        <div className={styles.button}><Button type="primary" onClick={this.handleSubmit}>Submit</Button></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BeverageOrderForm;