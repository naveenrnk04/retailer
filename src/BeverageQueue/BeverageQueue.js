import React, { Component } from 'react';
import classNames from 'classnames';

//Styles + Configs
import styles from './BeverageQueue.module.css';

class BeverageQueue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            beverageItems: [],
        }
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            this.state.beverageItems.map((data, i) => {
                var dt10s = new Date(data.OrderCreatedTimeStamp);
                dt10s.setSeconds(dt10s.getSeconds() + 10);
                var dt20s = new Date(data.OrderCreatedTimeStamp);
                dt20s.setSeconds(dt20s.getSeconds() + 20);
                if (dt10s < new Date() && !(dt20s < new Date())) {
                    data.IsBeingMixed = true;
                } else if (dt20s < new Date()) {
                    data.IsBeingMixed = false;
                    data.IsReadyToCollect = true;
                }
            })
            this.forceUpdate();
        }, 5)
    }

    componentWillMount() {
        this.props.newItem !== undefined && this.addNewBeverageItem(this.props.newItem);
    }

    addNewBeverageItem(newItem) {
        const Item = {
            "OrderCreatedTimeStamp": new Date(),
            "BeverageBarOrderId": "733416c1-89ed-46ab-9484-e88ce05960ad",
            "OrderedBeverage": {
                "BeverageId": newItem.beverageId,
                "Name": newItem.beverageName
            },
            "OrderQuantity": 1,
            "IsBeingMixed": false,
            "IsReadyToCollect": false,
            "IsCollected": false,
            "BeverageBarUserId": "U034AT9TN",
            "BeverageBarUserFirstName": newItem.name,
            "OrderDeliveredTimeStamp": "0001-01-01T00:00:00+00:00"
        };
        this.state.beverageItems.push(Item);
    }

    render() {

        return (
            <div className={styles.container}>
                <div className={styles.header}>BeverageQueue</div>
                <div className={classNames({ [styles.queueItems]: true, [styles.flexRow]: true })}>
                    <div className={styles.Item1}>
                        <div className={styles.header}>In The Queue</div>
                        {this.state.beverageItems.map((data, i) => {
                            if (data.IsBeingMixed === false && data.IsReadyToCollect === false) {
                                return (
                                    <div className={styles.beverageQueueItem} key={i}>
                                        <div className={styles.beverageName}>{data.OrderedBeverage.Name}</div>
                                        <div className={styles.userName}>{data.BeverageBarUserFirstName}</div>
                                    </div>
                                )
                            }
                        })}
                    </div>
                    <div className={styles.Item2}>
                        <div className={styles.header}>Being Mixed</div>
                        {this.state.beverageItems.map((data, i) => {
                            if (data.IsBeingMixed === true && data.IsReadyToCollect === false) {
                                return (
                                    <div className={styles.beverageQueueItem} key={i}>
                                        <div className={styles.beverageName}>{data.OrderedBeverage.Name}</div>
                                        <div className={styles.userName}>{data.BeverageBarUserFirstName}</div>
                                    </div>
                                )
                            }
                        })}
                    </div>
                    <div className={styles.Item3}>
                        <div className={styles.header}>Ready To Collect</div>
                        {this.state.beverageItems.map((data, i) => {
                            if (data.IsBeingMixed === false && data.IsReadyToCollect === true) {
                                return (
                                    <div className={styles.beverageQueueItem} key={i}>
                                        <div className={styles.beverageName}>{data.OrderedBeverage.Name}</div>
                                        <div className={styles.userName}>{data.BeverageBarUserFirstName}</div>
                                    </div>
                                )
                            }
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export default BeverageQueue;