// components



import React from 'react';
import { Router } from 'react-router-dom';
import App from './App';

export default function ({ history }) {
    return (
        <Router history={history}>
            <App />
        </Router>
    );
};