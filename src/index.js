import * as serviceWorker from './serviceWorker';
import '../node_modules/antd/dist/antd.css';
import dva from 'dva';
import { createBrowserHistory } from 'history';
import router from './router';
import beveragequeue from './models/beveragequeue';

// 1. Initialize
const app = dva({
    history: createBrowserHistory()
});

// 2.Models
app.model(beveragequeue);

// 3. Router
app.router(router);

// 4. Start
app.start('#root');

serviceWorker.unregister();
