const beverageMenuItemList = [

    {
        "Name": "Sparkling Cranberry Punch",
        "BeverageId": "bdc605f0-21c1-4ede-84d5-a878052dd983"
    },
    {
        "Name": "Raspberry Fizz",
        "BeverageId": "8abc64a1-8083-42bc-a048-eabe8d2f280f"
    },
    {
        "Name": "Virgin Frozen Margarita",
        "BeverageId": "51ba367c-a6d4-4e35-bc3f-319f48500aa8"
    },
    {
        "Name": "Iced Choclate Delight",
        "BeverageId": "08d7d6fb-c1a5-4807-81dc-3bb313b97f42"
    },
    {
        "Name": "Summer Punch",
        "BeverageId": "a94f2805-2a33-4fb2-b6c3-0e83f2e34812"
    },
	
];

export default beverageMenuItemList;