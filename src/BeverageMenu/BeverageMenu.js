import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//styles + config
import styles from './BeverageMenu.module.css';
import beverageMenuItemList from '../config/beverageMenu';


class BeverageMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.flexColumn}>
                    <div className={styles.header}>Beverage menu</div>
                    <div className={styles.menuItemList}>
                        {beverageMenuItemList.map((data, i) =>
                            <Link to="/orderform" key={i}>
                                <div className={styles.menuItem} value={data.BeverageId} >{data.Name}</div>
                            </Link>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default BeverageMenu;