import React, { Component } from 'react';

//styles
import styles from './BeverageHome.module.css';

//components
import BeverageMenu from '../BeverageMenu/BeverageMenu';
import BeverageQueue from '../BeverageQueue/BeverageQueue';

class BeverageHome extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className={styles.App}>
                <div className={styles.body}>
                    <BeverageQueue newItem={this.props.location.query && this.props.location.query} />
                </div>
                <div className={styles.leftMenu}>
                    <BeverageMenu />
                </div>
            </div>
        );
    }
}

export default BeverageHome;