import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';

//styles
import styles from './App.module.css';
import 'bootstrap/dist/css/bootstrap.css';

//Components
import BeverageHome from './BeverageHome/BeverageHome';
import BeverageOrderForm from './BeverageOrderForm/BeverageOrderForm';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    return (
      <div className={styles.mainBody}>
        <Router>
          <Switch>
            <Route exact path={`/`} component={BeverageHome} />
            <Route exact path={`/orderform`} component={BeverageOrderForm} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default connect()(App);
